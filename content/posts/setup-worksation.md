---
title: "Software installation on windows"
date: 2022-04-15T22:17:53+02:00
draft: false
tags: ["windows", "tools"]
---

In the linux world, package manager exist for quite a long time and allow to easily find, install and update softwares.
In the windows world, a few exists as well. 

One of them is [Scoop](https://scoop.sh/)
If you are a developer or any kind of IT technician working in an enterprise where windows is the platform of choice, this can help you make your work environment more confortable.

Follow the installation procedure on the website.
Once scoop is installed you can start installin software you need to work.
Here is an example installation script

```powershell
# adding the extras bucket to have access to all tool
scoop bucket add extras#Base stuff
scoop install 7zip git grep less touch wget

#better terminal
scoop install oh-my-posh posh-git consolez

#editors
scoop install obsidian vscode

#various tools
scoop install treesize-free q-dir greenshot hugo
```

Once all those are installed, some ocnfiguration is still needed
 
```powershell
echo 'oh-my-posh init pwsh --config="$(scoop prefix oh-my-posh)\themes\rudolfs-dark.omp.json" | Invoke-Expression' >> $PROFILE
```

Download and install system wide one of the [Nerd fonts](https://www.nerdfonts.com/font-downloads)

For example **"Sauce Code Pro Nerd Font"** is pretty nice.

Don't forget to configure gt

```powershell
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```